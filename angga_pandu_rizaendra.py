import argparse
import os, sys, glob
import json, re

parser = argparse.ArgumentParser(description="Log File Conversion to Text and JSON")

# parser.add_argument("--help", "-h", help="HELP", action="store_true")
parser.add_argument("source_path", type=str, help="Source File Path")
parser.add_argument("--output_path", "-o", type=str, help="Destination File Path")
parser.add_argument("--to", "-t", type=str, help="Conversion Name")

# input = source/file/path/error.log
# output = destination/file/path/error.json or destination/file/path/error.txt

args = parser.parse_args()

if args.to == 'json':
	if args.output_path == None:
		print("this is json conversion with flag -t without -o")
	
		input_file = args.source_path
		base_path = os.path.dirname(input_file)
		filename = os.path.basename(input_file)
		filename = filename.replace('.log', '.json')
		output_file = base_path + "\\" + filename

		if os.path.isfile(input_file) == True:
			with open(input_file, 'r', encoding='utf-8') as f:
					list_data = f.read()
					if "Response: " in list_data:
						response_string = re.search(r'(Response:\s*)(.*)(?=\(HttpClientUtil\))', list_data, re.DOTALL)
						response_obj = json.loads(response_string.group(2))
						
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(json.dumps(response_obj))
					else:
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(list_data)

		else:
			print("File Not Found")

	else:
		print("this is json conversion with flag -t and -o")
		input_file = args.source_path
		output_file = args.output_path
		base_path = os.path.dirname(output_file)
		full_filename = os.path.basename(output_file)
		
		name_ext = full_filename.split('.')
		filename = name_ext[0]
		ext_name = name_ext[1]

		if ext_name == 'json':

			if os.path.isfile(input_file) == True:
				with open(input_file, 'r', encoding='utf-8') as f:
					list_data = f.read()
					if "Response: " in list_data:
						response_string = re.search(r'(Response:\s*)(.*)(?=\(HttpClientUtil\))', list_data, re.DOTALL)
						response_obj = json.loads(response_string.group(2))
						
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(json.dumps(response_obj))
					else:
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(list_data)

			else:
				print("File Not Found")

		else:
			print("not a json extension")


elif args.to == 'text':
	if args.output_path == None:
		print("this is text conversion with flag -t TEXT without -o")
	
		input_file = args.source_path
		base_path = os.path.dirname(input_file)
		filename = os.path.basename(input_file)
		filename = filename.replace('.log', '.txt')
		output_file = base_path + "\\" + filename

		if os.path.isfile(input_file) == True:
			with open(input_file, 'r', encoding='utf-8') as f:
				list_data = f.read()

			with open(output_file, 'w', encoding='utf-8') as w:
				w.write(list_data)

		else:
			print("File Not Found")

	else:
		print("this is text conversion with flag -t TEXT with -o")
		input_file = args.source_path
		output_file = args.output_path
		base_path = os.path.dirname(output_file)
		full_filename = os.path.basename(output_file)
		
		name_ext = full_filename.split('.')
		filename = name_ext[0]
		ext_name = name_ext[1]
		
		if ext_name == 'txt':
		
			if os.path.isfile(input_file) == True:
				# print("filenya ada")

				with open(input_file, 'r', encoding='utf-8') as f:
					list_data = f.read()

				with open(output_file, 'w', encoding='utf-8') as w:
					w.write(list_data)

			else:
				print("File Not Found")

		else:
			print("Not a text extension")

else:
	if args.output_path == None:
		print("this is text conversion without flag -t and -o")
		input_file = args.source_path
		base_path = os.path.dirname(input_file)
		filename = os.path.basename(input_file)
		filename = filename.replace('.log', '.txt')
		output_file = base_path + "\\" + filename

		if os.path.isfile(input_file) == True:
			with open(input_file, 'r', encoding='utf-8') as f:
				list_data = f.read()

			with open(output_file, 'w', encoding='utf-8') as w:
				w.write(list_data)

		else:
			print("File Not Found")

	else:
		print("this is text conversion without flag -t and there is -o flag")
		input_file = args.source_path
		output_file = args.output_path
		base_path = os.path.dirname(output_file)
		full_filename = os.path.basename(output_file)
		
		name_ext = full_filename.split('.')
		filename = name_ext[0]
		ext_name = name_ext[1]
		
		if ext_name == 'txt':		
			if os.path.isfile(input_file) == True:
				with open(input_file, 'r', encoding='utf-8') as f:
					list_data = f.read()

				with open(output_file, 'w', encoding='utf-8') as w:
					w.write(list_data)

			else:
				print("File Not Found")

		elif ext_name == 'json':
			if os.path.isfile(input_file) == True:
				with open(input_file, 'r', encoding='utf-8') as f:
					list_data = f.read()
					if "Response: " in list_data:
						response_string = re.search(r'(Response:\s*)(.*)(?=\(HttpClientUtil\))', list_data, re.DOTALL)
						response_obj = json.loads(response_string.group(2))
						
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(json.dumps(response_obj))
					else:
						with open(output_file, 'w', encoding='utf-8') as w:
							w.write(list_data)

			else:
				print("File Not Found")

		else:
			print("Not a txt neither json extensions")

